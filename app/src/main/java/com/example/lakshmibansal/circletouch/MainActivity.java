package com.example.lakshmibansal.circletouch;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.lakshmibansal.circletouch.appConstants.AppConstants.*;


public class MainActivity extends Activity implements View.OnTouchListener {
    RelativeLayout layout;
    double xCoordinate,yCoordinate,centerX,centerY;
    TextView output;
    Button circle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        layout = (RelativeLayout) findViewById(R.id.touchLayout);
        layout.setOnTouchListener(this);
        output = (TextView)findViewById(R.id.value);
        circle = (Button) findViewById(R.id.circle);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN:
                getCoordinates(event);
                calculatePosition(circle);
                break;
            case MotionEvent.ACTION_UP:
                output.setText(INITIATE_TOUCH);
                break;
            case MotionEvent.ACTION_MOVE:
                getCoordinates(event);
                calculatePosition(circle);
                break;
        }
        return true;
    }
    public void onClick(View view){
        switch (view.getId()){
            case R.id.secondScreen:
                Intent intent = new Intent(MainActivity.this,MultipleCircles.class);
                startActivity(intent);
                break;
        }
    }
    private void calculatePosition(View view) {
        double radius = view.getWidth()/2;
        calculateCenter(view);
        if(calculateDistance()==radius){
            output.setText(ON_BOUNDRY);
        }
        else if(calculateDistance()>radius){
            output.setText(OUTSIDE_CIRCLE);
        }
        else if(calculateDistance()<radius){
            output.setText(INSIDE_CIRCLE);
        }
    }

    private void calculateCenter(View view) {
        centerX = view.getX()+view.getWidth()/2;
        centerY = view.getY() + view.getHeight()/2;
        Toast.makeText(this,"X : "+ centerX+" Y : "+centerY,Toast.LENGTH_SHORT).show();
    }

    private double calculateDistance() {
        double distance = Math.sqrt(Math.pow(xCoordinate-centerX,2)
                            +Math.pow(yCoordinate-centerY,2));
        return distance;
    }

    private void getCoordinates(MotionEvent event) {
        xCoordinate = event.getX();
        yCoordinate = event.getY();
    }
}
