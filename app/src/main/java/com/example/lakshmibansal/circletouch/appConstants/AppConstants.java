package com.example.lakshmibansal.circletouch.appConstants;


public interface AppConstants {
    String ON_BOUNDRY = "on the Boundry";
    String INSIDE_CIRCLE = "Inside circle";
    String OUTSIDE_CIRCLE = "outside circle";
    String INITIATE_TOUCH = "Touch the Circle";
    Boolean WHITE = false;
    Boolean PINK = true;
    int MAX_ROWS = 4;
    int MAX_COL = 5;
    int MIN_VALUE = 0;
    int MAX_VALUE = 9;
    int ONE =0;
    int TWO =1;
    int THREE =2;
    int FOUR =3;
    int FIVE =4;
    int SIX = 5;
    int SEVEN = 6;
    int EIGHT =7;
    int NINE =8;
    int NUM_CIRCLE = MAX_COL*MAX_ROWS;
    String INVALID_NUMBER_OF_ROWS_MESSAGE = "Number of rows should be less than or equal to "+MAX_ROWS;
    String INVALID_NUMBER_OF_COLS_MESSAGE = "Number of columns should be less than or equal to "+MAX_COL;
    String FIELDS_EMPTY = "Values can not be left blank";
}
