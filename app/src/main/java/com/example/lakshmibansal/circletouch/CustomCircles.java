package com.example.lakshmibansal.circletouch;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lakshmibansal.circletouch.adapter.CircleAdapter;
import com.example.lakshmibansal.circletouch.appConstants.Data;

import  static com.example.lakshmibansal.circletouch.appConstants.AppConstants.*;


public class CustomCircles extends Activity implements View.OnTouchListener {
    CircleAdapter adapter;
    GridView gridView;
    EditText row, column;
    int circleNumber = NUM_CIRCLE;
    int numRows, numColumns;
    RelativeLayout relativeLayout;
    double xCoordinate, yCoordinate, radius, finalDistance;
    TextView output;
    boolean[] flag;
    boolean touched = false;
    int currentCircle = NUM_CIRCLE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_circles);
        gridView = (GridView) findViewById(R.id.grid);
        adapter = new CircleAdapter(this);
        output = (TextView) findViewById(R.id.output);
        gridView.setAdapter(adapter);
        row = (EditText) findViewById(R.id.rows);
        column = (EditText) findViewById(R.id.columns);
        relativeLayout = (RelativeLayout) findViewById(R.id.touchLayout1);
        gridView.setOnTouchListener(this);

        flag = new boolean[NUM_CIRCLE];

    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create:
                createCircles();
                break;
        }
    }

    private void createCircles() {
        if (!row.getText().toString().equals("")) {
            numRows = Integer.parseInt(row.getText().toString());
            if (!column.getText().toString().equals("")) {
                numColumns = Integer.parseInt(column.getText().toString());
                if (numRows > MAX_ROWS) {
                    Toast.makeText(this, INVALID_NUMBER_OF_ROWS_MESSAGE, Toast.LENGTH_SHORT).show();
                }else if ( numColumns > MAX_COL) {
                    Toast.makeText(this, INVALID_NUMBER_OF_COLS_MESSAGE, Toast.LENGTH_SHORT).show();
                } else {
                    gridView.setNumColumns(numColumns);
                    int numCircles = numColumns * numRows;
                    if (adapter.getCount() > 0) {
                        adapter.circleNumber.clear();
                    }
                    for (int i = 0; i < numCircles; i++) {
                        Button newB = new Button(this);
                        newB.setBackgroundResource(R.drawable.round_button);
                        adapter.circleNumber.add(newB);
                        flag[i] = WHITE;
                    }
                    adapter.notifyDataSetChanged();
                    touched = true;
                }
            } else {
                Toast.makeText(this, FIELDS_EMPTY, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this,FIELDS_EMPTY, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (!adapter.isEmpty()) {
                    getXYCoordinate(event);
                    if (touched) {
                        radius = calculateRadius();
                        adapter.calculateCenters();
                        touched = false;
                    }
                    if(calculateDistance()<adapter.getCount())
                        changeColor(calculateDistance());
                }
                break;
            case MotionEvent.ACTION_MOVE:
               getXYCoordinate(event);
                if (touched) {
                    radius = calculateRadius();
                    adapter.calculateCenters();
                    touched = false;
                }
                if(calculateDistance()<adapter.getCount() && (circleNumber!=calculateDistance())){
                    changeColor(calculateDistance());
                    circleNumber = calculateDistance();
                }

            case MotionEvent.ACTION_UP:
                output.setText(R.string.touch_circle);
                break;
        }
        return true;
    }
    private double calculateRadius() {
        Button currentB = (Button) adapter.getItem(0);
        return (currentB.getWidth() / 2);
    }

    private void getXYCoordinate(MotionEvent event) {
        xCoordinate = event.getX();
        yCoordinate = event.getY();
    }
    private int calculateDistance() {
        int number = adapter.getCount();
        for(int i = MIN_VALUE;i<adapter.getCount();i++) {
            double distance = Math.sqrt(Math.pow(xCoordinate - Data.centerX[i], 2)
                    + Math.pow(yCoordinate - Data.centerY[i], 2));
            if(distance<radius){
                output.setText(INSIDE_CIRCLE + i);
                return  i;
            }
        }
        circleNumber = adapter.getCount();
        return number;
    }
    private void changeColor(int i) {
        Button currentB = (Button) adapter.getItem(i);
        if(flag[i] == WHITE){
            currentB.setBackgroundResource(R.drawable.touched_button);
            adapter.setButton(currentB);
            flag[i] = PINK;
        }else if(flag[i] == PINK){
            currentB.setBackgroundResource(R.drawable.round_button);
            adapter.setButton(currentB);
            flag[i] = WHITE;
        }
    }
}
