package com.example.lakshmibansal.circletouch;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.lakshmibansal.circletouch.appConstants.AppConstants.EIGHT;
import static com.example.lakshmibansal.circletouch.appConstants.AppConstants.FIVE;
import static com.example.lakshmibansal.circletouch.appConstants.AppConstants.FOUR;
import static com.example.lakshmibansal.circletouch.appConstants.AppConstants.INITIATE_TOUCH;
import static com.example.lakshmibansal.circletouch.appConstants.AppConstants.INSIDE_CIRCLE;
import static com.example.lakshmibansal.circletouch.appConstants.AppConstants.MAX_VALUE;
import static com.example.lakshmibansal.circletouch.appConstants.AppConstants.MIN_VALUE;
import static com.example.lakshmibansal.circletouch.appConstants.AppConstants.NINE;
import static com.example.lakshmibansal.circletouch.appConstants.AppConstants.ONE;
import static com.example.lakshmibansal.circletouch.appConstants.AppConstants.ON_BOUNDRY;
import static com.example.lakshmibansal.circletouch.appConstants.AppConstants.OUTSIDE_CIRCLE;
import static com.example.lakshmibansal.circletouch.appConstants.AppConstants.PINK;
import static com.example.lakshmibansal.circletouch.appConstants.AppConstants.SEVEN;
import static com.example.lakshmibansal.circletouch.appConstants.AppConstants.SIX;
import static com.example.lakshmibansal.circletouch.appConstants.AppConstants.THREE;
import static com.example.lakshmibansal.circletouch.appConstants.AppConstants.TWO;
import static com.example.lakshmibansal.circletouch.appConstants.AppConstants.WHITE;


public class MultipleCircles extends Activity implements View.OnTouchListener {
    RelativeLayout layout;
    double xCoordinate  ,yCoordinate;
    double[] centerX = new double[MAX_VALUE],centerY = new double[MAX_VALUE],radius = new double[MAX_VALUE];
    Boolean[] color = new Boolean[MAX_VALUE];
    TextView output;
    int circleNumber = MAX_VALUE;
    Button[] circle  = new Button[MAX_VALUE];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiple_circles);
        layout = (RelativeLayout) findViewById(R.id.touchLayout);
        layout.setOnTouchListener(this);
        output = (TextView)findViewById(R.id.value);
        circle[ONE] = (Button)findViewById(R.id.circle1);
        circle[TWO] = (Button)findViewById(R.id.circle2);
        circle[THREE] = (Button)findViewById(R.id.circle3);
        circle[FOUR] = (Button)findViewById(R.id.circle4);
        circle[FIVE] = (Button)findViewById(R.id.circle5);
        circle[SIX] = (Button)findViewById(R.id.circle6);
        circle[SEVEN] = (Button)findViewById(R.id.circle7);
        circle[EIGHT] = (Button)findViewById(R.id.circle8);
        circle[NINE] = (Button)findViewById(R.id.circle9);
        for(int i = MIN_VALUE;i<MAX_VALUE;i++) {
            color[i] = WHITE;
        }
    }
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        calculateRadius();
        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN:
                getCoordinates(event);
                if(calculateDistance()<MAX_VALUE)
                changeColor(calculateDistance());
                break;
            case MotionEvent.ACTION_UP:
                output.setText(INITIATE_TOUCH);
                break;
            case MotionEvent.ACTION_MOVE:
                getCoordinates(event);
                if(calculateDistance()<MAX_VALUE && (circleNumber!=calculateDistance())){
                    changeColor(calculateDistance());
                    circleNumber = calculateDistance();
                }
                break;
        }
        return true;
    }

    public void onClick(View view){
        switch (view.getId()){
            case R.id.thirdScreen:
                Intent intent = new Intent(this,CustomCircles.class);
                startActivity(intent);
                break;
        }

    }

    private void calculateRadius(){
      for(int i = MIN_VALUE;i<MAX_VALUE;i++){
            centerX[i]= circle[i].getX()+circle[i].getWidth()/2;
            centerY[i]= circle[i].getY()+circle[i].getHeight()/2;
            radius[i] = circle[i].getWidth()/2;
        }
    }

    private int calculateDistance() {
        int number = MAX_VALUE;
        for(int i = MIN_VALUE;i<MAX_VALUE;i++) {
            double distance = Math.sqrt(Math.pow(xCoordinate - centerX[i], 2)
                    + Math.pow(yCoordinate - centerY[i], 2));
             if(distance<radius[i]){
                output.setText(INSIDE_CIRCLE + i);
                 return  i;
            }
        }
        circleNumber = MAX_VALUE;
        return number;
    }

    private void changeColor(int i) {
        if(color[i] == WHITE){
            circle[i].setBackgroundResource(R.drawable.touched_button);
            color[i] = PINK;
        }else if(color[i] == PINK){
            circle[i].setBackgroundResource(R.drawable.round_button);
            color[i] = WHITE;
        }
    }

    private void getCoordinates(MotionEvent event) {
        xCoordinate = event.getX();
        yCoordinate = event.getY();
    }
}
